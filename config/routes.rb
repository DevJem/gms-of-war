Rails.application.routes.draw do
  resources :models
  devise_for :users
  # get 'profile/user/:id(.:format)' => 'profile#user'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # get '/login'  => 'profile#login', as: :login
  # get '/signup' => 'profile#signup', as: :signup
  # get '/profile/user' => 'profile#user'
  resources :profiles
  resources :guilds
  root 'profiles#index'

end
