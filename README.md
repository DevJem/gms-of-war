# GMs Of War

* Relevant links:
    * Production site - https://gms-of-war.herokuapp.com/
    * Gitlab Source - https://gitlab.com/DevJem/gms-of-war
    * Project Documentation and Plans - https://docs.google.com/document/d/1NvEwg4zld37RV4C2HJsVOudXEkRqUGBZNYtVkcPJD5k/edit?usp=sharing
    * Thorough Docs - https://docs.google.com/document/d/1SY2-wadBWgGUWO1MCq_HB7pK3apdJ8np9E51LG62GOA/edit?usp=sharing
    * Remaining Task List - https://docs.google.com/document/d/1nftHSFExmeT2BFfcYmZPciefpRURcXBcbVZdzGdYXOg/edit?usp=sharing
    
* Video Submissions:
    * Verification and Validation - https://youtu.be/53ZHzKngegE
    * Reusable Code - https://youtu.be/2sC274CWUg0
    * Databases - https://youtu.be/N2Hg8RO7ZTc
    * Architectural Requirements - https://youtu.be/RlCIdNl2egY
    * System in-use - https://youtu.be/tmjG10hZ1nY