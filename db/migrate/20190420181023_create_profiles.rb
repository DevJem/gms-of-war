class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string  :player_name
      t.integer :player_level
      t.integer :kingdom_status
      t.integer :troop_count
      t.integer :gold_donated
      t.integer :seals_donated
      t.integer :trophies_earned
      t.string  :invite_code
      t.integer :user_id
      t.integer :guild_id
      t.timestamps
    end
  end
end
