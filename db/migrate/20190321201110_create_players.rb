class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string  :player_name
      t.integer :player_level
      t.integer :kingdom_status
      t.integer :troop_count
      t.integer :gold_donated
      t.integer :seals_donated
      t.integer :trophies_earned
      t.string  :invite_code
      t.timestamps
      t.integer :user_id
      t.integer :guild_id
    end
  end
end
