class CreateGuilds < ActiveRecord::Migration[5.2]
  def change
    create_table :guilds do |t|
      t.string :guild_name
      t.integer :guild_rank
      t.timestamps
    end
  end
end
