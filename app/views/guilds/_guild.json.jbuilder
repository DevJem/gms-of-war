json.extract! guild, :id, :created_at, :updated_at
json.url guild_url(guild, format: :json)
