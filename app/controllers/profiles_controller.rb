class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  # before_action :get_token, only: [:show]

  @token = ""

  def login
  end

  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    today = Date.today
    @week = today.at_beginning_of_week

    values = {
      'NameCode' => 'DITHANIAL_BYWT',
      'functionName' => 'get_hero_profile',
      # 'serverToken' => @@token,
      'clientVersion' => '4.3.0'
   }

    headers = {
        content_type: 'application/json'
    }

    @response = RestClient.post 'https://pcmob.parse.gemsofwar.com/call_function', values, headers

    @response = JSON.parse(@response)
    @troop_count = 0
    @response["result"].each do |data, result|
      if data == "ProfileData"
        result.each do |key, value|
          if key == "Name"
            @username = value
            next
          end
          if key == "Level"
            @level = value
            next
          end
          if key == "NameCode"
            @invite_code = value
            next
          end
          if key == "GuildGoldContributedWeekly"
            @gold = value
            next
          end
          if key == "GuildSealsWeekly"
            @seals = value
            next
          end
          if key == "GuildTrophiesWeekly"
            @trophies = value
            next
          end
          if key == "GuildName"
            @guild = value
            next
          end
          if key == "NumKingdomsOwned"
            @kingdoms = value
            next
          end
          if key == "Troops"
            value.each do |count|
              @troop_count += 1
            end
            next
          end
        end
      end
      puts @troop_count
      # Profile.create(player_name: @username,
      #                player_level: @level,
      #                kingdom_status: @kingdoms,
      #                troop_count: @troop_count,
      #                gold_donated: @gold,
      #                seals_donated: @seals,
      #                trophies_earned: @trophies,
      #                invite_code: @invite_code,
      #                user_id: @profile.id,
      #                guild_id: 1)
    end

    # @username = @username.parse_JSON("result")
    # puts @username
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.fetch(:profile, {})
    end


end

# curl --header "Content-Type: application/json" \
#   --request POST \
#   --data "{
#         'clientVersion': '4.3.0',
#         'functionName': 'login_user',
#         'username': '4RNhuB1EYEBk',
#         'password': 'ed3xcfXuYQYZ'
#       }" \
#   https://pcmob.parse.gemsofwar.com/call_function